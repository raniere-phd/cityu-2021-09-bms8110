# Step 5 {-}

Let's load the libraries that we will use in this step.

```{r message=FALSE, warning=FALSE}
library(tidyverse)

library(pathview)
```

```{r message=FALSE, warning=FALSE}
package.version('tidyverse')
package.version('pathview')
```

Our gene set enrichment analysis
and
overrepresentation analysis,
see Table\ \@ref(tab:confirmed_go_bp),
reported some gene sets related with keratinization,
in other words, 
cells producing large amounts of keratin.
Cervix of healthy women
normally
does not produce keratin\ [@wentzSurvivalCervicalCancer1959]
so we will investigate this further.

Let's get a vector of all genes related with keratinization.

```{r}
keratin_genes <- discovery_go_bp %>%
  filter(
    str_detect(Gene.Set.Term, 'kera')
  ) %>%
  dplyr::select(Overlap.Gene) %>%
  unlist(use.names=FALSE) %>%
  str_flatten(';') %>%
  str_split(';') %>%
  unlist(use.names=FALSE) %>%
  unique()
```

Let's get the differentially expressed genes that are related with keratinization.

```{r}
keratin_deg <- validation %>%
  filter(Symbol %in% keratin_genes) %>%
  dplyr::select(Symbol, Name, logFC, P.Value, adj.P.Val)
```

(ref:keratinization) Differentially expressed genes related with keratinization.

```{r keratinization, echo=FALSE, message=FALSE, warning=FALSE}
keratin_deg %>%
  knitr::kable(
    "pipe",
    booktabs = TRUE,
    row.names=FALSE,
    caption = '(ref:keratinization)'
)
```


Based on the `adj.P.Val` of Table\ \@ref(tab:keratinization),
we decide to further investigate keratin 2 (KRT2).
