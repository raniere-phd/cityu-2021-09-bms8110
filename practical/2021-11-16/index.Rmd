---
title: "BMS8110 - Practical Class"
author:
- Raniere Gaia Costa da Silva
date: "`r Sys.Date()`"
output:
  html_document:
    df_print: kable
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Load libraries

```{r message=FALSE, warning=FALSE}
# BiocManager::install(c("DESeq2", "GO.db", "org.Hs.eg.db", 'HTSanalyzeR2'))
# devtools::install_github("CityUHK-CompBio/HTSanalyzeR2", dependencies=TRUE)
library(DESeq2)
library(GO.db)
library(org.Hs.eg.db)
library(HTSanalyzeR2)
```

# Differentially expressed genes

Let's get the name of the files that we will use.

```{r}
ctfiles <- list.files('data')
```

Let's read the files.

```{r}
WT1 <- read.table(
  paste0('data/', ctfiles[1]),
  sep='\t',
  header=FALSE,
  stringsAsFactors = FALSE,
  skip = 4
)[, 1:2]
WT2 <- read.table(
  paste0('data/', ctfiles[2]),
  sep='\t',
  header=FALSE,
  stringsAsFactors = FALSE,
  skip = 4
)[, 1:2]
TamR1 <- read.table(
  paste0('data/', ctfiles[3]),
  sep='\t',
  header=FALSE,
  stringsAsFactors = FALSE,
  skip = 4
)[, 1:2]
TamR2 <- read.table(
  paste0('data/', ctfiles[4]),
  sep='\t',
  header=FALSE,
  stringsAsFactors = FALSE,
  skip = 4
)[, 1:2]
```

Let's merge the files into a single data frame.

```{r}
cts <- cbind(
  WT1 = WT1[, 2],
  WT2 = WT2[match(WT1$V1, WT2$V1), 2],
  TamR1 = TamR1[match(WT1$V1, TamR1$V1), 2],
  TamR2 = TamR2[match(WT1$V1, TamR2$V1), 2]
)
```

Let's name the rows.

```{r}
rownames(cts) <- WT1$V1
```

Let's built DESeqDataSet's colData.


```{r}
col_data <- data.frame(
  condition=c(rep("WT", 2), rep("TamR", 2)),
  row.names = colnames(cts)
)
```

Let's create the DESeqDataSet object.

```{r}
dds <- DESeqDataSetFromMatrix(
  countData = cts,
  colData = col_data,
  design = ~ condition  # lazy load
)
```

Let's reduce the DESeqDataSet object to the most significant genes.

```{r}
featureData <- data.frame(
  gene=rownames(cts)
)
mcols(dds) <- DataFrame(mcols(dds), featureData)

dds$condition <- relevel(dds$condition, ref ="WT")

keep <- rowSums(counts(dds)) >= 10
dds <- dds[keep,]
```

Let's perform the data analysis.

```{r}
dds <- DESeq(dds)
res <- results(dds)

sum(res$padj < 0.05 & abs(res$log2FoldChange) > 1, na.rm = TRUE)
```

Let's save the differentially expressed genes as CSV file.

```{r}
DEGs <- res[res$padj <0.05 & abs(res$log2FoldChange) > 1, ]
write.csv(
  DEGs,
  file='DEGs.csv'
)
```

# Functional analysis

Let's create GO and KEGG gene set databases.

```{r}
GO_MF <- GOGeneSets(
  species="Hs",
  ontologies = c('MF')
)
GO_BP <- GOGeneSets(
  species="Hs",
  ontologies = c('BP')
)
PW_KEGG <- KeggGeneSets(species='Hs')
ListGSC <- list(
  GO_MF=GO_MF,
  GO_BP=GO_BP,
  PW_KEGG=PW_KEGG
)
```

Let's prepare the gene set enrichment analysis.

```{r}
data4enrich <- res$log2FoldChange
names(data4enrich) <- rownames(res)
```

Let's elect upregulated genes in Tamoxifen resistant MCF7 cells for overrepresentation analysis.

```{r}
hits <- rownames(res)[
  which(res$padj < 0.05 & res$log2FoldChange > 1)
]
```

Let's create the GSCA object.

```{r}
gsca <- GSCA(
  listOfGeneSetCollections = ListGSC,
  geneList = data4enrich,
  hits = hits
)
gsca <- preprocess(
  gsca,
  species='Hs',
  initialIDs='SYMBOL',
  keepMultipleMappings=TRUE,
  duplicateRemoverMethod='max',
  orderAbsValue=FALSE
)

gsca <- analyze(
  gsca,
  para=list(
    pValueCutoff=0.05,
    pAdjustMethod='BH',
    nPermutations=1000,
    minGeneSetSize=15,
    exponent=1
  ),
  doGSOA=TRUE,
  doGSEA=TRUE
)
```

Let's replace IDs with gene set terms.

```{r}
gsca <- appendGSTerms(
  gsca,
  goGSCs=c('GO_BP', 'GO_MF'),
  keggGSCs=c('PW_KEGG')
)
```

Let's lauch the web interface.

```{r eval=FALSE}
report(gsca)
```
