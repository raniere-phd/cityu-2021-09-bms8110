---
title: "BMS8110 - Practical Class"
author:
- Raniere Gaia Costa da Silva
date: "`r Sys.Date()`"
output:
  html_document:
    df_print: kable
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(ggplot2)
```

Open the `.Rproj` to set the working directory
or
the `setwd()` function.

Let's load the data:

```{r}
load('GC.miR200.rdata')
```

Let's inspect the data:

```{r}
head(dat)
```

Let's calculate the mean in the `miRmean` column:

```{r}
dat['miRmean'] <- (dat$miR200a.exp + dat$miR200b.exp + dat$miR429.exp) / 3
head(dat)
```

Let's test whether there is a significant difference between the mean of the miR-200 expression of the EMT subtypes and other subtypes.

```{r}
emt_p <- dat[dat$Subtype == 'EMT', 'miRmean']
emt_n <- dat[dat$Subtype != 'EMT', 'miRmean']
```

```{r}
EMT.exp.t.res <- t.test(
  emt_p,
  emt_n
)
EMT.exp.t.res

```

Conclusion: the mean of miR-200 expression is significantly lower in EMT subtype than other subtypes.

Let's test whether there is a significant difference between the mean of the miR-200 methylation level of the EMT subtypes and other subtypes.

```{r}
emt_p <- dat[dat$Subtype == 'EMT', 'miR200ab429.methy.mean']
emt_n <- dat[dat$Subtype != 'EMT', 'miR200ab429.methy.mean']
```

```{r}
EMT.methy.t.res <- t.test(
  emt_p,
  emt_n
)
EMT.methy.t.res
```

Conclusion: the mean of miR-200 methylation level is significantly higher in EMT subtype than other subtypes.

Let's test the significance of the Pearson Correlation between the expression levels and the methyltion levels

```{r}
exp.methy.cor <- cor.test(
  dat$miRmean,
  dat$miR200ab429.methy.mean,
  method="pearson")
exp.methy.cor
```

Conclusion: the mean of the miR-200 methylation level is negatively correlated with its expression.

Let's generate a scatter plot illustrating the correlation between miR-200 expression and methylation.

```{r}
ggplot(dat, aes(miRmean, miR200ab429.methy.mean)) +
  geom_point()
```

Let's fit a linear regression model.

```{r}
miR_lm <- lm(dat$miRmean ~ dat$miR200ab429.methy.mean)
miR_lm
```
```{r}
ggplot(dat, aes(miRmean, miR200ab429.methy.mean)) +
  geom_point() +
  geom_smooth(method='lm')
```

Let's save the previous plot.

```{r}
ggsave('plot.pdf')
```

Let's save the previous result as `.RData`.

```{r}
save(
  dat,
  EMT.exp.t.res,
  EMT.methy.t.res,
  exp.methy.cor,
  file='.RData'
)

